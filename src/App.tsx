import React, { useReducer } from 'react';

import DispatchContext from './context/DispatchContext';
import filterReducer from './reducers/filterReducer';
import todoReducer from './reducers/todoReducer';
import useCombinedReducer from './reducers/useCombinedReducers';

import { AddTodo, Filter, TodoList } from './components';

const App = () => {
  const [state, dispatch] = useCombinedReducer({
    filter: useReducer(filterReducer, 'ALL'),
    todos: useReducer(todoReducer, []),
  });

  const { filter, todos } = state;

  const filteredTodos = todos.filter((todo: Task) => {
    if (filter === 'ALL') {
      return true;
    }

    if (filter === 'COMPLETE' && todo.complete) {
      return true;
    }

    if (filter === 'INCOMPLETE' && !todo.complete) {
      return true;
    }

    return false;
  });

  return (
    <DispatchContext.Provider value={dispatch}>
      <Filter />

      <TodoList todos={filteredTodos} />

      <AddTodo />
    </DispatchContext.Provider>
  );
};

export default App;
