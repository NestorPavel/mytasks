import { createContext } from 'react';

const initialState = {
  state: {
    todos: [],
    filter: '',
  },

  dispatch: () => {},
};

const DispatchContext = createContext<GlobalReducerType>(initialState); 

export default DispatchContext;
