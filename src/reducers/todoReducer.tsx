const todoReducer: TodoReducerProps = (state, action) => {
  switch (action.type) {
    case 'TOOGLE_TODO':
      return state.map((todo: Task) => {
        return todo.id === action.id
          ? { ...todo, complete: !todo.complete }
          : todo;
      });

    case 'ADD_TODO':
      return state.concat({
        task: action.task,
        id: action.id,
        complete: false,
      });

    default:
      return state;
  }
};

export default todoReducer;
