import React, { useContext, useState } from 'react';
import { v4 as uuid } from 'uuid';

import DispatchContext from '../../context/DispatchContext';

const AddTodo = () => {
  const dispatch: any = useContext(DispatchContext);

  const [task, setTask] = useState('');

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    if (task) {
      dispatch({ type: 'ADD_TODO', task, id: uuid() });
    }

    setTask('');

    event.preventDefault();
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) =>
    setTask(event.target.value);

  return (
    <form onSubmit={handleSubmit}>
      <input type='text' value={task} onChange={handleChange} />

      <button type='submit'>Add Todo</button>
    </form>
  );
};

export default AddTodo;
