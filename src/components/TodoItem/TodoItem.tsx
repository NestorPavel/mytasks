import React, { useContext } from 'react';

import DispatchContext from '../../context/DispatchContext';

const TodoItem: React.FC<TodoItemProps> = ({ todo }) => {
  const dispatch: any = useContext(DispatchContext);

  const handleChange = () =>
    dispatch({
      type: 'TOOGLE_TODO',
      id: todo.id,
    });

  return (
    <li>
      <label>
        <input
          type='checkbox'
          checked={todo.complete}
          onChange={handleChange}
        />

        {todo.task}
      </label>
    </li>
  );
};

export default TodoItem;
