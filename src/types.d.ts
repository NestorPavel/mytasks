interface Task {
  id: string;
  task: string;
  complete: boolean;
}

interface AppProps {
  // dispatch: Function;
  // state: Task[];
}

interface TasksProviderProps {
  children: React.ReactNode;
}

interface FilterActionType {
  type: 'SHOW_ALL' | 'SHOW_COMPLETE' | 'SHOW_INCOMPLETE';
}

interface FilterReducerProps {
  (state: any, action: FilterActionType): 'ALL' | 'COMPLETE' | 'INCOMPLETE';
}

interface TodoActionType {
  type: 'TOOGLE_TODO' | 'ADD_TODO';
  task: string;
  id: string;
}

interface GlobalStateType {
  todos: Task[] | [];
  filter: string;
}

interface GlobalReducerType {
  state: GlobalStateType;
  dispatch: React.Dispatch<FilterActionType | TodoActionType> | Function;
}

interface TodoReducerProps {
  (state: Array<Task>, action: TodoActionType): Array<Task>;
}

interface TodoListProps {
  todos: Array<Task>;
}

interface TodoItemProps {
  todo: Task;
}
